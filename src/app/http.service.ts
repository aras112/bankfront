import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {AccountDto} from './dto/AccountDto';
import {TransferDto} from './dto/TransferDto';
import {CompatClient, Stomp} from '@stomp/stompjs';
import {mySocketFactory} from './messenger/messenger.component';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../environments/environment';
import {ExternalAccountDto} from './dto/ExternalAccountDto';
import {UserDto} from './dto/UserDto';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  accounts = new BehaviorSubject<Array<AccountDto>>([]);

  private http: HttpClient;
  private stompClient: CompatClient;
  private web = environment.web;

  constructor(http: HttpClient, public toast: ToastrService) {
    this.http = http;

    this.updateAccounts();

    this.stompClient = Stomp.over(mySocketFactory);
    console.log(this.stompClient);
    this.stompClient.connect('guest', 'guest', () => {
      this.stompClient.send('/api/aa', {}, '');
      this.stompClient.subscribe('/topic', () => {
        this.toast.info('Transfer accept', 'BANK', {timeOut: 2000});
        this.updateAccounts();
      });
    });
  }


  updateAccounts() {
    if (this.active()) {
      return this.http.get<Array<AccountDto>>(this.web + '/account/all').subscribe(value => {
        this.accounts.next(value);
      });
    }
  }

  transfer(transferDto: TransferDto) {
    this.http.post(this.web + '/transfer/send', transferDto).subscribe(value => {
      this.toast.success('transfer success', 'BANK');
    });
  }

  deleteAccountById(id: number) {
    this.http.delete(this.web + `/account/delete/${id}`).subscribe(value => {
      this.toast.success('delete success', 'BANK');
    });
  }

  getAccounts(): Observable<Array<AccountDto>> {
    this.updateAccounts();
    return this.accounts.asObservable();
  }

  getAccountById(id: number): Observable<AccountDto> {
    const account = this.http.get<AccountDto>(this.web + `/account/${id}`);
    return account;
  }

  getTransferOutByAccountId(id: number): Observable<Array<TransferDto>> {
    return this.http.get<Array<TransferDto>>(this.web + `/account/transfer/${id}`);
  }

  getTransferInByAccountId(id: number): Observable<Array<TransferDto>> {
    return this.http.get<Array<TransferDto>>(this.web + `/account/transferIn/${id}`);
  }

  addAccount(account: AccountDto) {
    this.http.post(this.web + '/account/add', account).subscribe(value => {
      this.toast.success('add success', 'BANK');
    });
  }

  login(login: string, password: string): Observable<number> {
    return this.http.post<number>(this.web + '/user/login', {login, password});
  }

  getAccountsByTransferId(id: number): Observable<Array<AccountDto>> {
    return this.http.get<Array<AccountDto>>(this.web + `/transfer/accounts/${id}`);
  }

  getAccountByNumber(accountNumber: string): Observable<AccountDto> {
    return this.http.get<AccountDto>(this.web + `/account/number/${accountNumber}`);
  }

  active(): boolean {
    return !(localStorage.getItem('user') === null || localStorage.getItem('user') === '');
  }

  getExternalAccount(): Observable<Array<ExternalAccountDto>> {
    return this.http.get<Array<ExternalAccountDto>>('https://restapi97.herokuapp.com/api/accounts');
  }

  registerUser(user: UserDto) {
  this.http.post(this.web + '/user/addUser', user).subscribe(value => this.toast.success('Account created', 'Bank'));
  }
}
