import {Component, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {GuardService} from '../guard.service';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  public active(): boolean {
    return !(localStorage.getItem('user') === null || localStorage.getItem('user') === '');
  }

  logOut() {
    localStorage.setItem('user', '');
  }

}
