import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpService} from '../http.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(public http: HttpService, public toasterService: ToastrService) {
  }

  ngOnInit() {
    this.form = new FormGroup({

      login: new FormControl(null),
      password: new FormControl(null)

    });
  }

  onSubmit() {
    console.log(this.form);
    localStorage.setItem('user', '');
    const isCorrect = this.http.login(this.form.value.login, this.form.value.password);

    isCorrect.subscribe(value => {
      if (value !== null) {
        localStorage.setItem('user', this.form.value.login + ':' + this.form.value.password);
        localStorage.setItem('id', value.toString());
        this.toasterService.success('correct', 'login', {tapToDismiss: true, closeButton: true});
      }
    });

  }

}
