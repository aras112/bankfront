import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(public toasterService: ToastrService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (localStorage.getItem('user') != null && !req.url.match('https://restapi97*')) {
      const headers = new HttpHeaders({Authorization: 'Basic ' + btoa(localStorage.getItem('user'))});
      req = req.clone({headers});
    }

    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse) => {
        console.log(err);
        this.toasterService.error(err.error.message, 'ERROR');
        return throwError(err);
      })
    );
  }
}
