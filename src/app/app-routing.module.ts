import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from './account/account.component';
import {TransferComponent} from './transfer/transfer.component';
import {AddAccountComponent} from './add-account/add-account.component';
import {EditAccountComponent} from './edit-account/edit-account.component';
import {AccountInfoComponent} from './account-info/account-info.component';
import {LoginComponent} from './login/login.component';
import {GuardService} from './guard.service';
import {MessengerComponent} from './messenger/messenger.component';
import {RegisterComponent} from './register/register.component';


const routes: Routes = [{
  path: 'accounts',
  component: AccountComponent,
  canActivate: [GuardService]
},
  {
    path: 'messenger',
    component: MessengerComponent,
    canActivate: [GuardService]
  },
  {
    path: 'transfer',
    component: TransferComponent,
    canActivate: [GuardService]
  },
  {
    path: 'addAccount',
    component: AddAccountComponent,
    canActivate: [GuardService]
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'editAccount',
    component: EditAccountComponent,
    canActivate: [GuardService]
  },
  {
    path: 'accountInfo/:id',
    component: AccountInfoComponent,
    canActivate: [GuardService]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '**',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
