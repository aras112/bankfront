import {Component, OnInit} from '@angular/core';
import {HttpService} from '../http.service';
import {AccountDto} from '../dto/AccountDto';
import {ExternalAccountDto} from '../dto/ExternalAccountDto';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  accounts: Array<AccountDto> = new Array<AccountDto>();
  externalAccounts: Array<ExternalAccountDto>;
  private http: HttpService;

  constructor(http: HttpService) {
    this.http = http;
    http.getAccounts().subscribe(value => this.accounts = value);
    http.getExternalAccount().subscribe(value => {
      this.externalAccounts = value;
      console.log(value);
    });
  }

  ngOnInit() {
  }

  delete(id: number) {
    this.http.deleteAccountById(id);
    this.accounts = this.accounts.filter(value => value.id !== id);
  }
}
