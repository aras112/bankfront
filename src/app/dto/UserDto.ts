export class UserDto {
  name: string;
  surname: string;
  age: number;
  email: string;
  password: string;
}
