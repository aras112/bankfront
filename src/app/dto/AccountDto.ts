export class AccountDto {
  id: number;
  userId: number;
  accountNumber: string;
  amount: string;
  currency: string;
  active: boolean;
}
