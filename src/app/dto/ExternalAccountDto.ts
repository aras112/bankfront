export class ExternalAccountDto {
  id: string;
  number: number;
  currency: string;
  owner: string;
}
