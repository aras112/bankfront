export class TransferDto {
  id: number;
  currency: string;
  amount: number;
  fromAccount: number;
  toAccount: number;
  accountNumberTo: string;
  accountNumberFrom: string;
  external: string;
}
