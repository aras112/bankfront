import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpService} from '../http.service';
import {AccountDto} from '../dto/AccountDto';

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent implements OnInit {

  public form: FormGroup;
  private http: HttpService;
  private account: AccountDto;

  constructor(http: HttpService) {
    this.http = http;
  }

  ngOnInit() {
    this.form = new FormGroup({
      accountNumber: new FormControl('12345678901234567890123456'),
      userId: new FormControl(1),
      amount: new FormControl(100),
      currency: new FormControl('PLN')
    });
  }

  onSubmit() {
    this.account = new AccountDto();
    this.account.accountNumber = this.form.value.accountNumber;
    this.account.amount = this.form.value.amount;
    this.account.userId = Number.parseInt(localStorage.getItem('id'), 10);
    this.account.currency = this.form.value.currency;
    this.http.addAccount(this.account);
  }

}
