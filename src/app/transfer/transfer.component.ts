import {Component, OnInit} from '@angular/core';
import {TransferDto} from '../dto/TransferDto';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validator, Validators} from '@angular/forms';
import {HttpService} from '../http.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  transfer: TransferDto;
  form: FormGroup;
  private http: HttpService;

  constructor(http: HttpService, public toast: ToastrService) {
    this.http = http;
  }

  ngOnInit() {
    this.transfer = new TransferDto();
    this.form = new FormGroup({
      amount: new FormControl('1', [Validators.pattern('^[0-9]{1,6}([.,\,][0-9]{1,2})?$'), Validators.required]),
      currency: new FormControl('EUR'),
      from: new FormControl('1', [Validators.required]),
      to: new FormControl('12345678901234567890123111', [Validators.minLength(26), Validators.required])
    });

    console.log(this.form.value.to);
  }

  onSubmit() {
    this.transfer.amount = this.form.value.amount;
    this.transfer.currency = this.form.value.currency;
    this.transfer.fromAccount = this.form.value.from;

    const account = this.http.getAccountByNumber(this.form.value.to);

    account.subscribe(value => {
      this.transfer.toAccount = value.id;
      this.http.transfer(this.transfer);
    });
  }

}
