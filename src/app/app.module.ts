import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BarComponent} from './bar/bar.component';
import {AccountComponent} from './account/account.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {TransferComponent} from './transfer/transfer.component';
import {AddAccountComponent} from './add-account/add-account.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EditAccountComponent} from './edit-account/edit-account.component';
import {AccountInfoComponent} from './account-info/account-info.component';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {InterceptorService} from './interceptor.service';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { MessengerComponent } from './messenger/messenger.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    BarComponent,
    AccountComponent,
    TransferComponent,
    AddAccountComponent,
    EditAccountComponent,
    AccountInfoComponent,
    LoginComponent,
    MessengerComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot()
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: InterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent],
  exports: [AppComponent]
})
export class AppModule {
}
