import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserDto} from '../dto/UserDto';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public form: FormGroup;
  private user: UserDto;
  private http: HttpService;

  constructor(http: HttpService) {
    this.http = http;
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      pass: new FormControl(null, Validators.minLength(6)),
      surname: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }

  submit() {
    this.user = new UserDto();

    this.user.age = 20;
    this.user.name = this.form.get('name').value;
    this.user.email = this.form.get('email').value;
    this.user.password = this.form.get('pass').value;
    this.user.surname = this.form.get('surname').value;
    this.http.registerUser(this.user);
  }

}
