import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AccountDto} from '../dto/AccountDto';
import {HttpService} from '../http.service';
import {TransferDto} from '../dto/TransferDto';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-account-info',
  templateUrl: './account-info.component.html',
  styleUrls: ['./account-info.component.css']
})
export class AccountInfoComponent implements OnInit {

  public transfersIn: Array<TransferDto>;
  public transfersOut: Array<TransferDto>;
  private route: ActivatedRoute;
  private id: number;
  private account: AccountDto;
  private http: HttpService;

  constructor(route: ActivatedRoute, http: HttpService) {
    this.route = route;
    this.http = http;
  }

  ngOnInit() {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.http.getAccountById(this.id).subscribe(value => this.account = value);
    this.http.getTransferOutByAccountId(this.id).pipe(
      tap(x => x.forEach(value => this.setTransferAccountNumber(value)))
    )
      .subscribe(value => {
        this.transfersIn = value;
      });

    this.http.getTransferInByAccountId(this.id)
      .subscribe(value => {
        this.transfersOut = value;
        value.forEach(value1 => this.setTransferAccountNumber(value1));
      });
  }

  setTransferAccountNumber(transfer: TransferDto) {
    if (transfer.external === null || transfer.external === '') {
      this.http.getAccountsByTransferId(transfer.id).subscribe(value => {
        transfer.accountNumberTo = value[1].accountNumber;
        transfer.accountNumberFrom = value[0].accountNumber;
      });
    } else {
      transfer.accountNumberTo = transfer.external;
      transfer.accountNumberFrom = transfer.external;
    }
    // this.http.getAccountsByTransferId(transfer.id).toPromise().then(value => transfer.accountNumber = value[1].accountNumber);
  }

}
