import {Component, OnInit} from '@angular/core';
import * as SockJS from 'sockjs-client';
import {environment} from '../../environments/environment';


@Component({
  selector: 'app-messenger',
  templateUrl: './messenger.component.html',
  styleUrls: ['./messenger.component.css']
})
export class MessengerComponent implements OnInit {

  ngOnInit() {
  }
}

export function mySocketFactory() {
  return new SockJS(environment.web + '/my');
}

